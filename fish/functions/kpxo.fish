function kpxo --wraps='keepassxc .config/main.kdbx&' --wraps='keepassxc .config/main.kdbx $argv&' --description 'alias kpxo keepassxc .config/main.kdbx $argv&'
  keepassxc .config/main.kdbx $argv&; 
end
